
SELECT acs_log__debug('/packages/sencha-freelance-translation/sql/postgresql/upgrade/upgrade-4.1.0.0.4-4.1.0.0.5.sql','');

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
    v_count integer;
begin
    select count(*) into v_count
    from         user_tab_columns
where lower(table_name) = 'im_freelancer_quality';
    IF 0 != v_count THEN return 0; END IF;

    create table im_freelancer_quality (
    	mapping_id           serial,
        project_id          integer not null
                            constraint im_trans_fl_quality_project_fk
                            references im_projects,
        user_id             integer not null
                            constraint im_trans_fl_quality_user_fk
                            references users,
        quality_id  integer
                            constraint im_trans_fl_quality_id_fk
                            references im_categories,
        rating_date         date
    );

    update im_categories set aux_int1 = 5 where category_id = 110;
    update im_categories set aux_int1 = 4 where category_id = 111;
    update im_categories set aux_int1 = 2 where category_id = 112;
    update im_categories set aux_int1 = 1 where category_id = 113;
    return 0;
end;
$$ language 'plpgsql';
select inline_0 ();
drop function inline_0 ();