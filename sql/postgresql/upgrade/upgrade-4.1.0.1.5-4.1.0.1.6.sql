SELECT acs_log__debug('/packages/sencha-freelance-translation/sql/postgresql/upgrade/upgrade-4.1.0.1.5-4.1.0.1.6.sql','');

update im_menus set enabled_p = 'f' where label = 'project_trans_tasks_assignments';

create or replace function inline_0 ()
returns integer as '
declare
        -- Menu IDs
        v_menu                  integer;
        v_project_menu          integer;

        -- Groups
        v_employees             integer;
        v_accounting            integer;
        v_senman                integer;
        v_companies             integer;
        v_freelancers           integer;
        v_proman                integer;
        v_admins                integer;
begin

    select group_id into v_admins from groups where group_name = ''P/O Admins'';
    select group_id into v_senman from groups where group_name = ''Senior Managers'';
    select group_id into v_proman from groups where group_name = ''Project Managers'';
    select group_id into v_accounting from groups where group_name = ''Accounting'';
    select group_id into v_employees from groups where group_name = ''Employees'';
    select group_id into v_companies from groups where group_name = ''Customers'';
    select group_id into v_freelancers from groups where group_name = ''Freelancers'';


    select menu_id
    into v_project_menu
    from im_menus
    where label=''project'';

    v_menu := im_menu__new (
        null,              -- p_menu_id
        ''acs_object'',  -- object_type
        now(),            -- creation_date
        null,              -- creation_user
        null,              -- creation_ip
        null,              -- context_id
        ''sencha-freelance-translation'',     -- package_name
        ''project_freelance_assignments'', -- label
        ''Assignments'',               -- name
        ''/sencha-freelance-translation/assignments?display_only_p=1'', -- url
        60,                  -- sort_order
        v_project_menu,  -- parent_menu_id
        ''[im_project_has_type [ns_set get $bind_vars project_id] "Translation Project"]'' -- p_visible_tcl
    );

    PERFORM acs_permission__grant_permission(v_menu, v_admins, ''read'');
    PERFORM acs_permission__grant_permission(v_menu, v_senman, ''read'');
    PERFORM acs_permission__grant_permission(v_menu, v_proman, ''read'');
    PERFORM acs_permission__grant_permission(v_menu, v_accounting, ''read'');
    PERFORM acs_permission__grant_permission(v_menu, v_employees, ''read'');
    PERFORM acs_permission__grant_permission(v_menu, v_companies, ''read'');
    
    return 0;
end;' language 'plpgsql';

select inline_0 ();

drop function inline_0 ();

-- Components for assignments

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
begin


    perform im_component_plugin__delete(plugin_id)
        from im_component_plugins
    where plugin_name in ('Project Assignments') and package_name = 'sencha-freelance-translation';

    perform im_component_plugin__new (
        null,                               -- plugin_id
        'im_component_plugin',              -- object_type
        now(),                              -- creation_date
        null,                               -- creation_user
        null,                               -- creation_ip
        null,                               -- context_id
        'Project Assignments',                 -- plugin_name
        'sencha-freelance-translation',              -- package_name
        'top',                             -- location
        '/sencha-freelance-translation/assignments',             -- page_url
        null,                               -- view_name
        5,                                 -- sort_order
        E'im_freelance_assignment_component -project_id $project_id -freelancer_ids $freelancer_ids -display_only_p $display_only_p'     -- component_tcl
    );

    perform acs_permission__grant_permission(
        plugin_id,
        (select group_id from groups where group_name = 'Employees'),
        'read')
    from im_component_plugins
where plugin_name in ('Project Assignments')
    and package_name = 'sencha-freelance-translation';

    return 1;

end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
begin


    perform im_component_plugin__delete(plugin_id)
        from im_component_plugins
    where plugin_name in ('Project Assignees') and package_name = 'sencha-freelance-translation';

    perform im_component_plugin__new (
        null,                               -- plugin_id
        'im_component_plugin',              -- object_type
        now(),                              -- creation_date
        null,                               -- creation_user
        null,                               -- creation_ip
        null,                               -- context_id
        'Project Assignees',                 -- plugin_name
        'sencha-freelance-translation',              -- package_name
        'right',                             -- location
        '/intranet/projects/view',             -- page_url
        null,                               -- view_name
        2,                                 -- sort_order
        E'im_freelance_project_assignees_component -project_id $project_id'     -- component_tcl
    );

    perform acs_permission__grant_permission(
        plugin_id,
        (select group_id from groups where group_name = 'Employees'),
        'read')
    from im_component_plugins
where plugin_name in ('Project Assignees')
    and package_name = 'sencha-freelance-translation';

    return 1;

end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();