# /packages/sencha-freelance-translation/tcl/sencha-freelance-translation-procs.tcl
#


ad_library {
    Common procedures to implement translation freelancer functions:
	@author malte.sussdorff@cognovis.de
}

ad_proc -public im_freelance_trans_main_task_type {
	-freelancer_id
	{-company_id ""}
} {
	Get the main task type of a freelancer
	
	@param company_id Company for which we check the task type. If even, look across all companies
	@param task_type Default task type to use if none is definitive
} {
	
	set worked_with_list_of_lists [im_freelance_trans_worked_with_customer -freelancer_id $freelancer_id -customer_id $company_id]
	set sorted_worked_with [lsort -integer -decreasing -index 1 $worked_with_list_of_lists]

	if {$company_id eq ""} {
		# Look across all companies, take the first one found
		set task_type [lindex [lindex $sorted_worked_with 0] 0]
	} else {
		set customer_elements [lsearch -index 0 -all -inline $sorted_worked_with "*_customer"]
		set task_type [lindex [lindex $customer_elements 0] 0]
	}
	return $task_type
}

ad_proc -public freelance_main_translators_component {
	{-company_id ""}
	{-user_id ""}
} {
	Returns the component for active projects
} {
	set params [list [list freelancer_id $user_id] [list customer_id $company_id]]
	set result [ad_parse_template -params $params "/packages/sencha-freelance-translation/lib/main-translators"]
	return [string trim $result]
}


ad_proc -public freelance_main_translators_update {
	{-worked_for 20}
} {
	This is still missing a check if the main translator has already been assigned. Should probably be limited to company as well.
} {
	
	db_foreach customer {
		select distinct company_id as customer_id, object_id_two as freelancer_id, source_language_id, target_language_id from (select project_id, im_user_worked_for_company(object_id_two,company_id) as worked_for, company_id, object_id_two from acs_rels r, im_projects p, users u where r.object_id_two = u.user_id and r.object_id_one =p.project_id and p.start_date > now() -interval '1 year') f, im_trans_tasks t where f.worked_for > :worked_for and f.project_id = t.project_id and (trans_id=object_id_two or edit_id = object_id_two)
	} {
		db_dml "insert into im_trans_main_freelancer (customer_id, freelancer_id, source_language_id, target_language_id) values(row.customer_id, row.freelancer_id, row.source_language_id, row.target_language_id)"
	}
}

ad_proc -public im_freelance_package_create {
	-trans_task_ids
	-package_type_id
	{-package_status_id ""}
	{-freelance_package_name ""}
} {
	Create a freelance package
	
	@trans_task_ids list of task_ids for which to create the package
	@task_type_id Task Type to use
} {
	# We need to generate the freelance_package
	set p_task_id [lindex $trans_task_ids 0]
	set project_id [db_string project_id "select project_id from im_trans_tasks where task_id = :p_task_id" -default ""]

	if {$project_id eq ""} {
	    # If we can't find the project we can't create the freelance_package_id
	    return 0
	} else {
	set user_id [ad_conn user_id]
	set ip_address [ad_conn peeraddr]
	
	if {$freelance_package_name eq ""} {
		db_1row project_info "select project_name, im_name_from_id(target_language_id) as target_language,task_name from im_projects p, im_trans_tasks tt where tt.project_id = p.project_id and tt.task_id = :p_task_id"
		set freelance_package_name "${project_name}_${target_language}_[im_category_from_id -translate_p 0 $package_type_id]"
		set existing_count [db_string count "select count(freelance_package_name) from im_freelance_packages where freelance_package_name like '${freelance_package_name}%'" -default 0]
		if {$existing_count >0} {
			# In case we already have the name, append an _1 or higher behind it
			set freelance_package_name "${freelance_package_name}_$existing_count"
		}
	}
	set freelance_package_id [db_string assignment "select im_freelance_package__new(
		:user_id,		-- creation_user
		:ip_address,		-- creation_ip
		:package_type_id,
		:package_status_id,
		:project_id,
		:freelance_package_name)"]
		
	# For the trans tasks add the entries into the mapping table
	foreach trans_task_id $trans_task_ids {
		db_dml add_mapping "insert into im_freelance_packages_trans_tasks (freelance_package_id,trans_task_id) values (:freelance_package_id, :trans_task_id)"
	}
    
    catch {
        callback im_freelance_packages_after_create -task_ids $trans_task_ids
    }

	return $freelance_package_id
	}
}

ad_proc -public im_freelance_package_file_create {
	-freelance_package_id
	-file_path
	{-freelance_package_file_name ""}
	{-package_file_type_id ""}
} {
	Create a freelance package file
	
	@param freelance_package_id Package where the file belongs to
	@param file_path Path where the file is residing. If a file is already present there, then we just update the values
} {
	set user_id [ad_conn user_id]
	set ip_address [ad_conn peeraddr]

	# Check if the file already exists
	set freelance_package_file_id [db_string file_exists "select freelance_package_file_id from im_freelance_package_files
		where freelance_package_id = :freelance_package_id and file_path = :file_path" -default ""]
	if {$freelance_package_file_id eq ""} {
		set freelance_package_file_id [db_string create_file "select im_freelance_package_file__new(:user_id,
			:ip_address,
			:freelance_package_id,
			:freelance_package_file_name,
			:package_file_type_id,
			620,
			:file_path) from dual
		"]
	} else {
		db_dml update_package_file "update im_freelance_package_files
			set freelance_package_file_name = :freelance_package_file_name,
				package_file_type_id = :package_file_type_id,
				package_file_status_id = 621
				where freelance_package_file_id = :freelance_package_file_id"
		db_dml update_acs_object "update acs_objects set last_modified = now(), modifying_user = :user_id where object_id = :freelance_package_file_id"
	}
	return $freelance_package_file_id
}

ad_proc -public im_freelance_assignment_create {
	{-freelance_package_id ""}
	{-task_ids ""}
	{-assignee_id}
	{-assignment_type_id}
	{-assignment_status_id "4222"}
	{-assignment_units "0"}
	{-uom_id ""}
	{-rate ""}
	{-start_date ""}
	{-end_date ""}
	{-ignore_min_price_p 0}
	{-assignment_comment ""}
} {
	Create a new package for multiple tasks

	@param freelance_package_id
	@param task_ids Trans Tasks for which this assignment is used
	@param assignee_id User who will work on this task
	@param assignment_type_id Task Type 'Intranet Trans Task Type' which is supposed to be done in this assignment
	@param assignment_status_id Status of this assignment. Defaults to accepted.
	@param assignment_units How many Hours/Source words etc. are in this assignment. Value after running the task through the trados matrix.
	@param uom_id Unit of measure for this assignment. Defaults to tasks uom_id
	@param rate Rate for this assignment. Calculate to the default rate for the freelancer if missing
	@param start_date Expected start_date for this assignment in \"YYYY-MM-DD HH24:MI\" format.
	@param end_date Deadline for this assignment in  \"YYYY-MM-DD HH24:MI\" format
	@param assignment_comment e.g. comment, note, additional information
} {

	if {$freelance_package_id eq "" && $task_ids eq ""} {
	    ns_log Error "im_freelance_assignment_create: Missing tasks or package - We can't process this without freelance_package_id or task_ids"
	    return ""
	    ad_script_abort
	}
	
	# Check if we maybe already have an assignment for this package_id
	if {$freelance_package_id ne ""} {
	    set assignment_id [db_string assignment "select max(assignment_id) from im_freelance_assignments where freelance_package_id = :freelance_package_id and assignee_id = :assignee_id" -default ""]
	} else {
	    set freelance_package_ids [db_list packages "select distinct fp.freelance_package_id from im_freelance_packages_trans_tasks fpt, im_freelance_packages fp where trans_task_id in ([template::util::tcl_to_sql_list $task_ids]) and package_type_id = :assignment_type_id and fpt.freelance_package_id = fp.freelance_package_id"]
	    if {$freelance_package_ids eq ""} {
		set assignment_id ""
	    } elseif {[llength $freelance_package_ids] eq 1} {
		set freelance_package_id [lindex $freelance_package_ids 0]
		set assignment_id [db_string assignment "select max(assignment_id) from im_freelance_assignments where freelance_package_id = :freelance_package_id" -default ""]
	    } else {
		ns_log Error "im_freelance_assignment_create: We have multiple packages for the tasks $task_ids  - $freelance_package_ids. Please try to creat the assignment for each of the packages"
		return ""
		ad_script_abort
	    }
	}

	if {$assignment_id eq ""} {
	    
	    if {$freelance_package_id ne ""} {
		set task_ids [db_list tasks "select trans_task_id from im_freelance_packages_trans_tasks fpt, im_freelance_packages fp where package_type_id = :assignment_type_id and fpt.freelance_package_id = fp.freelance_package_id and fp.freelance_package_id = :freelance_package_id"]
	    } else {
		set freelance_package_id [im_freelance_package_create -package_type_id $assignment_type_id \
					      -trans_task_ids $task_ids]
	    }
	    # For the basic information, use the first task
	    set task_id [lindex $task_ids 0]
	    
	    # Find the material_id.
	    db_1row get_task_info "select task_uom_id, p.project_id, tt.source_language_id, tt.target_language_id, subject_area_id, task_type_id from im_trans_tasks tt, im_projects p where p.project_id = tt.project_id and task_id = :task_id"
	    
	    set file_type_id ""
	    
	    set material_id [im_material_create_from_parameters -material_uom_id $task_uom_id -material_type_id [im_material_type_translation]]
	    
	    # Get the company of the freelancer
	    set provider_id [db_string company_id "select c.company_id
			from
				im_companies c,
				acs_rels r
			where
				r.object_id_two = :assignee_id
			and r.object_id_one = c.company_id order by company_id desc limit 1" -default ""]
	    
	    if {$provider_id eq ""} {
		set provider_id [im_company_freelance]
	    }
	    
	    # Get the task type of this assignment to we can calculate the  units
	    set task_type [im_category_from_id -translate_p 0 $assignment_type_id]
	    
	    if {$assignment_units eq "0"} {
		foreach task_id $task_ids {
		    # Calculate the assignment units
		    db_1row task_matrix "select match_x,match_rep,match100,match95,match85,match75,match50,match0,
						match_perf,match_cfr,match_f95,match_f85,match_f75,match_f50,locked
					from im_trans_tasks where task_id = :task_id"

		    set task_units [im_trans_trados_matrix_calculate $provider_id \
					$match_x $match_rep $match100 $match95 $match85 $match75 $match50 $match0 \
					$match_perf $match_cfr $match_f95 $match_f85 $match_f75 $match_f50 $locked $task_type]
		    set assignment_units [expr $assignment_units + $task_units]
		}
	    }
	    
	    # Default to the tasks unit of measure. We would only
	    # Change that if we e.g. have a 600 source words for the task but allow
	    # The editor to be billed by the hour
	    if {$uom_id eq ""} { set uom_id $task_uom_id}
	    
	    if {!$ignore_min_price_p} {
		
		# ---------------------------------------------------------------
		#  Minimum Price handling
		# ---------------------------------------------------------------
		
		# Find the currency for the provider by looking at the prices
		set currency ""
		if {[im_table_exists "im_trans_prices"]} {
		    db_0or1row currency "select currency, count(*) as num_prices from im_trans_prices where company_id = :provider_id group by currency order by num_prices desc limit 1"
		}
		
		if {$currency eq ""} {
		    set currency [ad_parameter -package_id [im_package_cost_id] "DefaultCurrency" "" "EUR"]
		}
		
		set total_rate_info [im_translation_best_rate \
					 -provider_id $provider_id \
					 -task_type_id $assignment_type_id \
					 -subject_area_id $subject_area_id \
					 -target_language_id $target_language_id \
					 -source_language_id $source_language_id \
					 -task_uom_id $task_uom_id \
					 -currency $currency \
					 -ignore_min_price_p $ignore_min_price_p \
					 -task_sum $assignment_units]
		
		# Split the rate information up into the components
		set total_billable_units [lindex $total_rate_info 0]
		set total_uom_id [lindex $total_rate_info 1]
		set total_rate [lindex $total_rate_info 2]
		
		if {[im_uom_unit] eq $total_uom_id} {
		    # We found a language where the tasks together are below the
		    # Minimum Price.
		    
		    set assignment_units 1
		    set uom_id $total_uom_id
		    set material_id [im_material_create_from_parameters -material_uom_id $total_uom_id -debug 1]
		    set rate $total_rate
		    
		}
		
	    }
	    
	    set assignment_name "[im_material_name -material_id $material_id] for [person::name -person_id $assignee_id] "
	    set user_id [ad_conn user_id]
	    set ip_address [ad_conn peeraddr]

	    ns_log Notice "Create assignment .... $material_id, $uom_id, $rate"

	    set assignment_id [db_string assignment "select im_freelance_assignment__new(
	    		:user_id,		-- creation_user
			:ip_address,		-- creation_ip
			:assignment_name,
			:freelance_package_id,
			:assignee_id,
			:assignment_type_id,
			:assignment_status_id,
			:material_id,
			:assignment_units,
			:uom_id,
			:rate,
			:start_date,
			:end_date,
			:assignment_comment )"]
	} else {
	    if {$freelance_package_id ne ""} {
		set task_ids [db_list tasks "select trans_task_id from im_freelance_packages_trans_tasks fpt, im_freelance_packages fp where package_type_id = :assignment_type_id and fpt.freelance_package_id = fp.freelance_package_id and fp.freelance_package_id = :freelance_package_id"]
	    }
	    
	    # For the basic information, use the first task
	    set task_id [lindex $task_ids 0]
	    
	    # Find the material_id.
	    db_1row get_task_info "select task_uom_id, p.project_id, tt.source_language_id, tt.target_language_id, subject_area_id, task_type_id from im_trans_tasks tt, im_projects p where p.project_id = tt.project_id and task_id = :task_id"
	    
	    if {$uom_id eq ""} { set uom_id $task_uom_id}

	    db_dml update_assignment "update im_freelance_assignments set assignment_status_id = :assignment_status_id, assignment_units = :assignment_units, uom_id = :uom_id, start_date = :start_date, end_date = :end_date, rate = :rate, assignment_comment = :assignment_comment where assignment_id = :assignment_id"
	}
	
	return $assignment_id
}

ad_proc -public im_freelance_create_purchase_orders {
	-assignment_id
	{ -user_id "" }
} {
	Create the purchase orders for an assignment.
	
	If the assignment has units, then those will be taken along with the uom_id for a total price across all tasks, otherwise
	we calculate the task values
	
	@param assignment_id ID of the assignment for which we create the purchase order
	
	@author malte.sussdorff@cognovis.de
} {
	# ---------------------------------------------------------------
	# Defaults & Security
	# ---------------------------------------------------------------
    if {$user_id eq ""} {
	    set user_id [ad_maybe_redirect_for_registration]
	}

	set target_cost_status_id [im_cost_status_created]
	set target_cost_type_id [im_cost_type_po]

	set todays_date [db_string get_today "select to_char(sysdate,'YYYY-MM-DD') from dual"]

	db_1row assignment_info "select assignment_type_id, freelance_package_id, assignee_id,
		material_id as assignment_material_id, purchase_order_id, assignment_units,
		uom_id as assignment_uom_id, rate as assignment_rate, start_date, end_date as assignment_end_date,
		im_name_from_id(assignment_type_id) as action
		from im_freelance_assignments where assignment_id = :assignment_id"

	db_1row project_info "select project_cost_center_id as cost_center_id,p.project_id
		from im_projects p, im_freelance_packages fp
		where fp.project_id = p.project_id
		and fp.freelance_package_id = :freelance_package_id"
		
	set user_locale [lang::user::locale -user_id $assignee_id]
	set locale $user_locale
	
	set po_created_p 0
	if {$purchase_order_id ne ""} {
		# Check if the purchase order hasn't been deleted / replaced
		set po_created_p [db_string po_valid "select 1 from im_costs where cost_id = :purchase_order_id and cost_status_id not in (3812,3813,3818)" -default 0]
	}

	if {$po_created_p} {
		return $purchase_order_id
		ad_script_abort
	}


	# ---------------------------------------------------------------
	# Create the purchase order
	# ---------------------------------------------------------------

	set provider_id [db_string select_company {
		select  c.company_id as provider_id
		from	    acs_rels r,
				im_companies c
		where	r.object_id_one = c.company_id
		and     r.object_id_two = :assignee_id
		limit 1
	} -default 0]
	
	set invoice_nr [im_next_invoice_nr -cost_type_id $target_cost_type_id -cost_center_id $cost_center_id]
	set invoice_id [im_new_object_id]
	set company_id [im_company_internal]
	

	# Check if we have a company for this freelancer, otherwise we can't create the purchase order
	if { 0 == $provider_id || $company_id eq $provider_id || [im_user_is_employee_p $assignee_id]} {
		return ""
		ad_script_abort
	}

	# Info about the provider
	db_1row select_company {
			select  company_id as provider_id,
					default_tax,
					default_payment_method_id,
					default_po_template_id,
					payment_term_id,
					vat_type_id
			from	    im_companies c
			where	company_id = :provider_id
	}
	
	
	if {"" == $default_po_template_id} {
		# Get a sensible default from the internal company
		set template_id [db_string internal_template "select default_po_template_id from im_companies where company_id = :company_id" -default ""]
	} else {
		set template_id $default_po_template_id
	}
	
	# Get the payment days from the company
	set payment_days ""
	if {$payment_term_id ne ""} {
		set payment_days [db_string payment_days "select aux_int1 from im_categories where category_id = :payment_term_id" -default ""]
	}
	if {$payment_days eq ""} {
		set payment_days [ad_parameter -package_id [im_package_cost_id] "DefaultCompanyInvoicePaymentDays" "" 30]
	}
	
	set tax_format [im_l10n_sql_currency_format -style simple]
	set note ""
	
	# Find the currency for the provider by looking at the prices
	set currency ""
	if {[im_table_exists "im_trans_prices"]} {
		db_0or1row currency "select currency, count(*) as num_prices from im_trans_prices where company_id = :provider_id group by currency order by num_prices desc limit 1"
	}
	
	if {$currency eq ""} {
		set currency [ad_parameter -package_id [im_package_cost_id] "DefaultCurrency" "" "EUR"]
	}
	
	# Set the old default vat to the vat_type_id.
	set default_vat [db_string vat "select aux_int1 from im_categories where category_id = :vat_type_id" -default ""]

	db_transaction {
		set invoice_id [db_exec_plsql create_invoice {
				select im_invoice__new (
					:invoice_id,		-- invoice_id
					'im_invoice',		-- object_type
					now(),			-- creation_date
					:user_id,		-- creation_user
					'[ad_conn peeraddr]',	-- creation_ip
					null,			-- context_id
					:invoice_nr,		-- invoice_nr
					:company_id,		-- company_id
					:provider_id,	-- provider_id
					:assignee_id,	-- company_contact_id
					now(),		    -- invoice_date
					:currency,			-- currency
					:template_id,	-- invoice_template_id
					:target_cost_status_id,	-- invoice_status_id
					:target_cost_type_id,		-- invoice_type_id
					:default_payment_method_id,	-- payment_method_id
					:payment_days,		-- payment_days
					0,			    -- amount
					to_number(:default_vat,:tax_format),			-- vat
					to_number(:default_tax,:tax_format),			-- tax
					:note			-- note
				)
		}]
	
		db_dml update_costs "
			update im_costs
			set
				project_id	= :project_id,
				cost_name	= :invoice_nr,
				customer_id	= :company_id,
				cost_nr		= :invoice_id,
				provider_id	= :provider_id,
				cost_status_id	= :target_cost_status_id,
				cost_type_id	= :target_cost_type_id,
				cost_center_id	= :cost_center_id,
				template_id	= :template_id,
				payment_days	= :payment_days,
				vat		= to_number(:default_vat,:tax_format),
				tax		= to_number(:default_tax,:tax_format),
				variable_cost_p = 't',
				currency	= :currency,
				payment_term_id = :payment_term_id,
				vat_type_id     = :vat_type_id,
				delivery_date = :assignment_end_date
			where
				cost_id = :invoice_id
		"
	
		# Add the link between the project and the invoice
		set rel_id [db_exec_plsql create_rel "      select acs_rel__new (
			 null,             -- rel_id
			 'relationship',   -- rel_type
			 :project_id,      -- object_id_one
			 :invoice_id,      -- object_id_two
			 null,             -- context_id
			 null,             -- creation_user
			 null             -- creation_ip
	  )"]
	}
	
	# ---------------------------------------------------------------
	# Get the billable units and units of measure
	# ---------------------------------------------------------------
	set sort_order 0
	
	# Get the task_units. if they are not the same as the assignment units we need to add a special line item
	set task_units [db_string task_units "select sum(task_units) from im_trans_tasks tt, im_freelance_packages_trans_tasks fptt
where fptt.trans_task_id = tt.task_id
and fptt.freelance_package_id = :freelance_package_id" -default 0]

	db_foreach task_tasks "select task_type_id, source_language_id, target_language_id,
		edit_end_date, trans_end_date, proof_end_date, task_id, task_name, task_uom_id
		from im_trans_tasks tt, im_freelance_packages_trans_tasks fptt
		where fptt.trans_task_id = tt.task_id
		and fptt.freelance_package_id = :freelance_package_id
	
	" {

	   set file_type_id ""
	   set task_type_id $assignment_type_id

		# ---------------------------------------------------------------
		# Get the billable units
		# ---------------------------------------------------------------

		switch $action {
			trans - edit - proof {
				array set provider_matrix [im_trans_trados_matrix -task_type $action $provider_id]

				db_1row billable_units "
					select round((tt.match_x * $provider_matrix(x) +
							tt.match_rep * $provider_matrix(rep) +
							tt.match_perf * $provider_matrix(perf) +
							tt.match_cfr * $provider_matrix(cfr) +
							tt.match100 * $provider_matrix(100) +
							tt.match95 * $provider_matrix(95) +
							tt.match85 * $provider_matrix(85) +
							tt.match75 * $provider_matrix(75) +
							tt.match50 * $provider_matrix(50) +
							tt.match0 * $provider_matrix(0) +
							tt.match_f95 * $provider_matrix(f95) +
							tt.match_f85 * $provider_matrix(f85) +
							tt.match_f75 * $provider_matrix(f75) +
							tt.match_f50 * $provider_matrix(f50) +
							tt.locked * $provider_matrix(locked)
						),0)   as po_billable_units,
						tt.task_uom_id as po_task_uom_id
					from im_trans_tasks tt
					where task_id = :task_id
						and (
							tt.task_uom_id = [im_uom_s_word]
							and tt.match100 is not null
						)
					UNION
						select tt.task_units as po_billable_units,
						   tt.task_uom_id as po_task_uom_id
						from	im_trans_tasks tt
						where task_id = :task_id
						and (
							tt.task_uom_id != [im_uom_s_word]
							or tt.match100 is null
						)
					"
			}
			default {
				db_1row billable_units "
						select  tt.billable_units as po_billable_units,
								tt.task_uom_id as po_task_uom_id
						from    im_trans_tasks tt
						where   tt.task_id = :task_id"
			}
		}
		

		# Title is there - add specifics
		switch $task_type_id {
			86 {
				set task_date_pretty [lc_time_fmt $assignment_end_date "%x %X" $locale]
			}
			88 {
				set task_date_pretty [lc_time_fmt $edit_end_date "%x %X" $locale]
			}
			93 {
				set task_date_pretty [lc_time_fmt $trans_end_date "%x %X" $locale]
			}
			95 {
				set task_date_pretty [lc_time_fmt $proof_end_date "%x %X" $locale]
			}
			default {set task_date_pretty [lc_time_fmt $assignment_end_date "%x %X" $locale]}
		}

		set item_name "[im_category_from_id -locale $locale $task_type_id]: $task_name ([im_category_from_id -locale $locale $source_language_id] -> [im_category_from_id -locale $locale $target_language_id]) Deadline: \"$task_date_pretty CET\""

		# ---------------------------------------------------------------
		# Create the line items
		# ---------------------------------------------------------------
		set item_id [db_nextval "im_invoice_items_seq"]
		
		# Reset rate if we have a different number of assignment units.
		if {$assignment_units ne $task_units} {
			set line_assignment_rate "0"
			set po_billable_units "0"
		} else {
			set line_assignment_rate $assignment_rate
		}
	
		set task_material_id [im_material_create_from_parameters -material_uom_id $task_uom_id]
		incr sort_order
		
		set insert_invoice_items_sql "
				INSERT INTO im_invoice_items (
					item_id, item_name,
					project_id, invoice_id,
					item_units, item_uom_id,
					price_per_unit, currency,
					sort_order, item_type_id,
					item_material_id,
					item_status_id, description, task_id,
					item_source_invoice_id
				) VALUES (
					:item_id, :item_name,
					:project_id, :invoice_id,
					:po_billable_units, :task_uom_id,
					:line_assignment_rate, :currency,
					:sort_order, :task_type_id,
					:task_material_id,
					null, '', :task_id,
					null
				)"
		db_dml insert_invoice_items $insert_invoice_items_sql
	}
	
	if {$assignment_units ne $task_units} {

		# Check if we have only one line item
		set num_line_items [db_string line_items "select count(*) from im_invoice_items where invoice_id = :invoice_id" -default 0]

		if {$num_line_items > 1} {
			# add a line item with the total price
			set item_id [db_nextval "im_invoice_items_seq"]
			set task_name "[im_category_from_id $assignment_type_id]"
			
			incr sort_order
			set insert_invoice_items_sql "
				INSERT INTO im_invoice_items (
					item_id, item_name,
					project_id, invoice_id,
					item_units, item_uom_id, item_material_id,
					price_per_unit, currency,
					sort_order, item_type_id,
					item_status_id, description, task_id
				) VALUES (
					:item_id, :task_name,
					:project_id, :invoice_id,
					:assignment_units, :assignment_uom_id, :assignment_material_id,
					:assignment_rate, :currency,
					:sort_order, :assignment_type_id,
					null, '', null
				)"
			
			db_dml insert_ass_price_items $insert_invoice_items_sql
		} else {
			set line_item_id [db_string line_item "select item_id from im_invoice_items where invoice_id = :invoice_id"]
			db_dml update_line_item "update im_invoice_items set item_units = :assignment_units, item_uom_id = :assignment_uom_id,
				item_material_id = :assignment_material_id, price_per_unit = :assignment_rate where item_id = :line_item_id"
		}
	}

	# Recalculate and update the invoice amount
	im_invoice_update_rounded_amount \
		-invoice_id $invoice_id \
		-discount_perc 0 \
		-surcharge_perc 0

	im_audit -object_type "im_invoice" -object_id $invoice_id -action after_create -status_id $target_cost_status_id -type_id $target_cost_type_id

	db_dml record_po_assignment "update im_freelance_assignments set purchase_order_id = :invoice_id where assignment_id = :assignment_id"
	
	return $invoice_id
}

ad_proc -public migrate_task_assignments {
	-project_id
	{-assignee_id ""}
} {
	Migrate the task_assignments from im_trans_tasks to im_trans_task_assignments

	Empty the *_id and *_date fields
} {

	set task_types [list]
	db_foreach task_type "select lower(column_name) as type_date from user_tab_columns where lower(table_name) = 'im_trans_tasks' and lower(column_name) like '%_end_date'" {
		set task_type [lindex [split $type_date "_"] 0]
		set task_type_id($task_type) [im_category_from_category -category $task_type -category_type "Intranet Trans Task Type"]
		lappend task_types $task_type
	}

	set task_type_from ""
	set update_list [list]

	set start_date [db_string start_date "select to_char(start_date,'YYYY-MM-DD HH24:MI') from im_projects where project_id = :project_id"]

	foreach type $task_types {


		db_foreach task "select ${type}_id,
			to_char(${type}_end_date,'YYYY-MM-DD HH24:MI') as ${type}_end_date,
			sum(task_units) as task_units, task_uom_id, array_to_string(array_agg(task_id), ',') as task_ids
		from im_trans_tasks where project_id = :project_id group by ${type}_id, ${type}_end_date, task_uom_id" {

			if {[set ${type}_id] ne ""} {
				if {[set ${type}_id] >0} {
					if {$assignee_id eq "" || $assignee_id eq [set ${type}_id]} {
						set assignment_id [im_freelance_assignment_create \
							-task_ids [split $task_ids ","] \
							-assignee_id [set ${type}_id] \
							-assignment_type_id $task_type_id($type) \
							-assignment_units $task_units \
							-start_date $start_date \
									   -end_date [set ${type}_end_date] \
									   -uom_id $task_uom_id
						]
					}
				}
			}
		}
	}
	return $task_types
}

ad_proc -public im_freelance_assignment_component {
	-project_id
	-freelancer_ids
	-display_only_p
    {-user_id ""}
	{-return_url ""}
} {
	Return the assignment component
} {
    set params [list  [list project_id] [list freelancer_ids] [list display_only_p] [list user_id]]
    set result [ad_parse_template -params $params "/packages/sencha-freelance-translation/lib/assignments"]
}


ad_proc -public im_freelance_packages_component {
	-project_id
	-display_only_p
	{-return_url ""}
} {
	Return the packages component
} {
	set params [list  [list project_id] [list display_only_p]]
	set result [ad_parse_template -params $params "/packages/sencha-freelance-translation/lib/packages"]
}

ad_proc -public im_freelance_project_status_component {
	-project_id
	{-return_url ""}
} {
	Return the packages component
} {
	set params [list  [list project_id]]
	set result [ad_parse_template -params $params "/packages/sencha-freelance-translation/lib/project-status"]
}

ad_proc -public im_freelance_project_assignees_component {
    -project_id
    {-user_id ""}
} {
	Returns the component for project assignees
} {
    set params [list [list project_id $project_id] [list user_id $user_id]]
	set result [ad_parse_template -params $params "/packages/sencha-freelance-translation/lib/project-assignees"]
	return [string trim $result]
}


ad_proc -public im_freelance_assignee_projects_component {
	-assignee_id
	{-assignment_status_ids ""}
} {
	Returns the component for project assignees
} {
	set params [list [list assignee_id $assignee_id] [list assignment_status_ids $assignment_status_ids]]
	set result [ad_parse_template -params $params "/packages/sencha-freelance-translation/lib/assignee-projects"]
	return [string trim $result]
}



ad_proc -public im_project_id_from_assignment_id {
    -assignment_id
} { 
    Simple procedure which returns project_id of assignment
    It is useful as we do not have project_id column in `im_freelance_assignments` table
} {
    set project_id [db_string project_id_from_assignment_id_sql "select project_id from im_freelance_packages fp, im_freelance_assignments fa where fa.freelance_package_id = fp.freelance_package_id and fa.assignment_id =:assignment_id limit 1" -default 0]
    return $project_id
}


ad_proc -public im_assignment_previous_stage_id {
    -assignment_id
} {
    Procedure which returns id ('Intranet Trans Task Type') of previous stage for given assignment (@param assignment_id)
} {
    set assignment_type_id [db_string assignment_type_id "select assignment_type_id from im_freelance_assignments where assignment_id =:assignment_id" -default 0]
    set assignment_type_name [db_string assignment_type_name "select category from im_categories where category_id =:assignment_type_id" -default ""]
    if {$assignment_type_id ne 0} {
        set project_id [im_project_id_from_assignment_id -assignment_id $assignment_id]
        set project_type_id [db_string project_type_id_sql "select project_type_id from im_projects where project_id =:project_id" -default 0]
        set project_type_name [im_category_from_id $project_type_id]
        set project_stages_list [db_string project_stages_list_sql "select aux_string1 from im_categories where category_id =:project_type_id" -default ""]
        # Just in case we convert assignment_type_name to be lowercased
        set assignment_type_name [string tolower $assignment_type_name]
        set assignment_stage_in_project [lsearch $project_stages_list $assignment_type_name]
        if {$assignment_stage_in_project eq 0} {
        	return -1;
        } else {
        	set previous_assignment_stage [expr $assignment_stage_in_project -1]
        	set previous_assignment_stage_name [lindex $project_stages_list $previous_assignment_stage]
        	set previous_assignment_stage_id [db_string previous_assignment_stage_id_sql "select category_id from im_categories where category =:previous_assignment_stage_name" -default ""]
        	return $previous_assignment_stage_id
        }
    }
}


ad_proc -public im_assignment_previous_stage_assignment_id {
    -assignment_id
} {
    Procedure which returns id of assignment of previous stage for given assignment (@param assignment_id)
} {

    set previous_stage_id [im_assignment_previous_stage_id -assignment_id $assignment_id]
    set project_id [im_project_id_from_assignment_id -assignment_id $assignment_id]

    # Get the package_ids for the previous_stage and compare the tasks
    set previous_freelance_package_ids [db_list previous_packages "select freelance_package_id from im_freelance_packages where project_id = :project_id and package_type_id = :previous_stage_id"]

    if {[llength $previous_freelance_package_ids] eq 0} {
	# No previous package found, so can't do this
	set previous_assignment_ids [list]
    } elseif {[llength $previous_freelance_package_ids] eq 1} {
	set previous_assignment_ids [db_string freelancers "select assignment_id from im_freelance_assignments  where freelance_package_id = :previous_freelance_package_ids and assignment_status_id not in (4223,4227,4228,4230)" -default ""]
    } else {
	# Need to look up the tasks for the freelance_package_id of the assignment and found out how many different freelancers worked on them.
	set assignment_task_ids [db_list tasks "select trans_task_id from im_freelance_packages_trans_tasks ptt, im_freelance_assignments fa 
       			where fa.freelance_package_id = ptt.freelance_package_id
       			and assignment_id = :assignment_id"]
	    
	set previous_assignment_ids [db_list freelancers "select distinct assignment_id from im_freelance_assignments 
	    where freelance_package_id in (select distinct freelance_package_id from im_freelance_packages_trans_tasks fptt
					   where fptt.trans_task_id in ([template::util::tcl_to_sql_list $assignment_task_ids])
					   and fptt.freelance_package_id in ([template::util::tcl_to_sql_list $previous_freelance_package_ids]))
	    and assignment_status_id not in (4223,4227,4228,4230)"]
    }
    
    return $previous_assignment_ids
}



ad_proc -public im_assignment_stage_of_assignment {
    -assignment_id
} {
    Procedure which returns index ('Intranet Trans Task Type') of stage for given assignment (@param assignment_id)
} {

    set assignment_type_id [db_string assignment_type_id "select assignment_type_id from im_freelance_assignments where assignment_id =:assignment_id" -default 0]
    set assignment_type_name [db_string assignment_type_name "select category from im_categories where category_id =:assignment_type_id" -default ""]
    if {$assignment_type_id ne 0} {
        set project_id [im_project_id_from_assignment_id -assignment_id $assignment_id]
        set project_type_id [db_string project_type_id_sql "select project_type_id from im_projects where project_id =:project_id" -default 0]
        set project_type_name [im_category_from_id $project_type_id]
        set project_stages_list [db_string project_stages_list_sql "select aux_string1 from im_categories where category_id =:project_type_id" -default ""]
        # Just in case we convert assignment_type_name to be lowercased
        set assignment_type_name [string tolower $assignment_type_name]
        set assignment_stage_in_project [lsearch $project_stages_list $assignment_type_name]
        return $assignment_stage_in_project
    }
}


ad_proc -public im_assignment_freelancers_working_ids {
    -project_id
    {-package_type_id ""}
} {
    Procedure which returns ids of freelancer who has assignments on given project (project searched with @param project_id)
    @param package_type_id filters assignments to gicen package type
    Example: If we add package_type_id of 4220 (Trans), we will receive list of freelancers ids who are working on Trans assignments
} {
    # Check if package_type_id was provided
    if {$package_type_id eq ""} {
        set package_sql ""
    } else {
        set package_sql "and package_type_id =:package_type_id" 
    }

    set freelancers_working_on_project_ids [list]
    set project_packages [db_list project_packages_sql "select freelance_package_id from im_freelance_packages where project_id=:project_id $package_sql"]
    # We exclude assignments which have status: 4223(Denied), 4220(Assignment Deleted)
    set excluded_statuses_ids [list 4223 4230]
    if {[llength $project_packages] > 0} {
        set project_not_removed_assignments [db_list project_not_removed_assignments_sql "select assignee_id from im_freelance_assignments where freelance_package_id in ([template::util::tcl_to_sql_list $project_packages]) and assignment_status_id not in ([template::util::tcl_to_sql_list $excluded_statuses_ids])"]
        # Exclude duplicate
        set project_not_removed_assignments_unique [lsort -unique $project_not_removed_assignments]
        return $project_not_removed_assignments_unique
    }
}


ad_proc -public im_assignment_is_rating_fl_possible {
    -assignment_id
} { 
   Procedure checks if rating freelancer is possible for given assignment (found with @param assignment_id)
   First it checks assignment stage of project (e.g. Trans or Edit), as we can only rate assignments starting from second stage
   In addition procedure checks if only single freelancer worked on that package
   returns 1 in case rating is possible, returns 0 if rating is not possible
}  {

    set previous_assignment_ids [im_assignment_previous_stage_assignment_id -assignment_id $assignment_id]
    if { $previous_assignment_ids eq "" } {
	set rating_fl_possible_p 0
    } else {

	# Check if we have only one previous freelancer
	set previous_freelancer_ids [db_list freelancers "select distinct assignee_id from im_freelance_assignments where assignment_id in ([template::util::tcl_to_sql_list $previous_assignment_ids])"]
	if {[llength $previous_freelancer_ids] eq 1} {
	    set rating_fl_possible_p 1
	} else {
	    set rating_fl_possible_p 0
	}
    }
    return $rating_fl_possible_p
}

ad_proc -public im_assignment_quality_report_new {
	{-creation_user_id ""}
	{-subject_area_id ""}
	-assignment_id
	-quality_type_id
   	-quality_level_id
   	{-comment ""}
} {
	Create a new quality report for an assignment

	@param creation_user_id Who created the report
	@param subject_area_id Subject area for which this report was done, default to project subject_area_id
	@param assignment_id Assignment for which this report was created
	@param quality_level_id Level of the quality (references 'Intranet Quality')
	@param quality_type_id Type of the report (references  'Intranet Translation Quality Type')
    @param comment for that quality report
} {

	if {$creation_user_id eq ""} {
		set creation_user_id [ad_conn user_id]
	}


	# Check that we pass on an assignment_id

	set assignment_p [db_string assignment_p "select 1 from im_freelance_assignments where assignment_id = :assignment_id" -default 0]

	if {$assignment_p} {

		if {$subject_area_id eq ""} {
			set project_id [im_project_id_from_assignment_id -assignment_id $assignment_id]
			set subject_area_id [db_string subject_area_id "select subject_area_id from im_projects where project_id = :project_id"]
		}

		set report_id [db_string create_report "select im_assignment_quality_report__new(null,'im_assignment_quality_report',now(),:creation_user_id,null,:assignment_id,:quality_type_id,:quality_level_id,:subject_area_id, :comment) from dual;"]
		return $report_id
	} else {
		return ""
	}
}



ad_proc -public im_assignment_quality_rating {
	-freelancer_id
	{-assignment_id ""}
	{-subject_area_id ""}
	{-quality_type_id ""}
} {
	Returns the quality rating as an average of all individual reports

	@freelancer_id Freelancer for whom we get the rating
	@assignment_id Rating for a single assignment. Might be useful to display in the project assignments table
	@subject_area_id Rating for a specific subject area (to identify freelancers good in a certain topic)
	@quality_type_id Rating for a specific type of skill.
} {
	set query_sql "select avg(aux_int1) from im_categories c,acs_objects o, im_assignment_quality_reports r, im_assignment_quality_report_ratings rr, im_freelance_assignments f where 
	rr.report_id = r.report_id and c.category_id = rr.quality_level_id and o.context_id = f.assignment_id and f.assignee_id = :freelancer_id"

	if {$assignment_id ne ""} {
		append query_sql " and f.assignment_id = :assignment_id"
	}

	if {$subject_area_id ne ""} {
		append query_sql " and r.subject_area_id = :subject_area_id"
	}

	if {$quality_type_id ne ""} {
		append query_sql " and r.quality_type_id = :quality_type_id"
	}

	set rating [db_string rating $query_sql -default ""]
}


