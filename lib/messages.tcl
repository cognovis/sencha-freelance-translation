# packages/intranet-mail/lib/messages.tcl

## Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
# Expects the following optional parameters (in each combination):
#
# recipient        - to filter mails for a single recipient (which can
# be both a company or a person)
# sender           - to filter mails for a single sender
# object           - to filter mails for a object_id
# party            - filter for the recipient, which is actually a sender or recipient
# page             - to filter the pagination
# page_size        - to know how many rows show (optional default to 10)
# show_filter_p    - to show or not the filters in the inlcude, default to "t"
# elements         - a list of elements to show in the list template. If not provided will show all elements.
#                    Posible elemets are: sender recipient subject object file_ids body sent_date

ad_page_contract {

    @author Nima Mazloumi
    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
} -query {
    {messages_orderby:optional "sent_date,desc"}
    elements:optional
} -properties {
    show_filter_p
    acs_mail_log:multirow
    context:onevalue
}

set show_filter_p 0
set page_title [ad_conn instance_name]
set context [list "index"]
set mail_url [export_vars -base "/sencha-freelance-translation/email" -url {project_id assignee_id}]

set show_filter_p "t"

set tracking_url [apm_package_url_from_key "sencha-freelance-translation"]

# Wich elements will be shown on the list template
set rows_list [list]
if {![exists_and_not_null elements] } {
    set rows_list [list sender {} subject {} sent_date {} body {}]
} else {
    foreach element $elements {
	lappend rows_list $element
	lappend rows_list [list]
    }
}

set mail_context_ids [db_list assignments "select assignment_id from im_freelance_assignments fa, im_freelance_packages fp
		where fa.freelance_package_id = fp.freelance_package_id
		and fa.assignee_id = :assignee_id
		and fp.project_id = :project_id"]
	
set context_sql_where "context_id in ([template::util::tcl_to_sql_list $mail_context_ids])"

set filters [list \
		 sender {
		     label "[_ intranet-mail.Sender]"
		     where_clause "sender_id = :sender"
		 } \
		 mail_context_ids {
		     label "[_ intranet-mail.Context_id]"
		     where_clause "$context_sql_where"
		 }
	    ]

set recipient_where_clause ""

template::list::create \
    -name messages \
    -selected_format normal \
    -multirow messages \
    -key acs_mail_log.log_id \
    -orderby_name "messages_orderby" \
    -page_size 30 \
    -page_flush_p 1 \
    -page_query_name "messages_pagination" \
    -row_pretty_plural "[_ intranet-mail.messages]" \
    -elements {
	sender {
	    label "[_ intranet-mail.Sender]"
	    display_template {
		@messages.sender_name;noquote@
	    }
	}
	recipient {
	    label "[_ intranet-mail.Recipient]"
	    display_template {
		@messages.recipient;noquote@
	    }
	}
	subject {
	    label "[_ intranet-mail.Subject]"
	    display_template {
		    @messages.subject;noquote@
	    }
	}
	context_id {
	    label "[_ intranet-mail.Context_id]"
	    display_template {
		<a href="@messages.context_url@">@messages.context_id@</a>
	    }
	}
	file_ids {
	    label "[_ intranet-mail.Files]"
	    display_template {@messages.download_files;noquote@}
	}
	body {
	    label "[_ intranet-mail.Body]"
	    display_template {
		    @messages.message_body;noquote@
	    }
	}
	sent_date {
	    label "[_ intranet-mail.Sent_Date]"
	    display_template {
		    @messages.sent_date;noquote@
	    }
	}
    } -orderby {
	sender {
	    orderby sender_id
	    label "[_ intranet-mail.Sender]"
	}
	subject {
	    orderby subject
	    label "[_ intranet-mail.Subject]"
	}
	sent_date {
	    orderby sent_date
	    label "[_ intranet-mail.Sent_Date]"
	}
    } -formats {
        normal {
            label "Table"
            layout table
            row $rows_list
        }
    } -filters $filters \

db_multirow -extend { file_ids context_url sender_name message_url message_body recipient package_name package_url url_message_id download_files} messages select_messages { } {

    if {[exists_and_not_null sender_id]} {
        set sender_name "[party::name -party_id $sender_id]"
    } else {
        set sender_name $from_addr
    }
    
    set message_url [export_vars -base "${tracking_url}one-message" -url {log_id return_url}]
    set reciever_list $to_addr
    set reciever_list2 [db_list get_recievers {select recipient_id from acs_mail_log_recipient_map where type ='to' and log_id = :log_id and recipient_id is not null}]
    
    foreach recipient_id $reciever_list2 {
        lappend reciever_list "[party::name -party_id $recipient_id]</a>"
    }

    set recipient [join $reciever_list "<br>"]
    
    set package_name ""
    set package_url ""

    set count 0
    while {[regexp {^(.*?)\t?=\?[^\?]+\?Q\?(.*?)\?=\n?(.*?)$} $subject match before quoted after] && $count < 5} {
        incr count
        set result ""
        for { set i 0 } { $i < [string length $quoted] } { incr i } {
            set current [string index $quoted $i]
            if {$current == "="} {
                incr i
                set high [string index $quoted $i]
                incr i
                set low [string index $quoted $i]
                set current [binary format H2 "$high$low"]
            } elseif {[string eq $current "_"]} {
                set current " "
            }
            append result $current
        }
        set subject "$before$result$after"
    }
    
    db_foreach files {} {
        append download_files "<a href=\"[export_vars -base "${tracking_url}download/$title" -url {version_id}]\">$title</a><br>"
    }

    set context_url "/o/$context_id"
    set sent_date [lc_time_fmt $sent_date "%x %X"]
    set message_body "<a href='$message_url' title='[_ intranet-mail.View_full_message]'>[_ intranet-mail.View]</a>"

    if {![views::viewed_p -object_id $log_id -user_id [ad_conn user_id]]} {
	    set sent_date "<b>$sent_date</b>"
	    set subject "<b>$subject</b>"
	    set sender_name "<b>$sender_name</b>"
   	    set message_body "<b>$message_body</b>"
    }
}

if {![exists_and_not_null mail_url]} {
    if {[exists_and_not_null mail_context_ids]} {
	    set object_id [lindex $mail_context_ids 0]
	set mail_url [export_vars -base "${tracking_url}mail" -url {object_id return_url}]
    } else {
	set mail_url ""
    }
}
ad_return_template
