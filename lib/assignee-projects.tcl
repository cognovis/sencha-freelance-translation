# ---------------------------------------------------------------
# Retrieve the projects
#
# Freelancers can either see the projects with tasks
# And / Or open requests
#
# ---------------------------------------------------------------


set element_list [list]
lappend element_list project_nr
lappend element_list {
	label "[_ intranet-core.Project_nr]"
	display_template {
		@multirow.project_nr_pretty;noquote@
	}
}

lappend element_list project_name
lappend element_list {
	label "[_ intranet-core.Project_name]"
}

set user_id [im_require_login]

# ---------------------------------------------------------------
# Work Amount
# If we only have one unit of measure, don't split
# ---------------------------------------------------------------


if {![exists_and_not_null assignment_status_ids]} {
	# Accepted, Work Started And Work Delivered (but not accepted)
	set assignment_status_ids [list 4222 4224 4225 4229]
}

# Get the different uom_ids
set uom_ids [db_list get_uoms "select distinct uom_id from im_freelance_assignments fa
	where fa.assignee_id = :assignee_id
	and fa.assignment_status_id in ([template::util::tcl_to_sql_list $assignment_status_ids])"]

lappend element_list work_amount

if {[llength $uom_ids] ne 1} {
	lappend element_list {
		label "[_ sencha-freelance-translation.Work_amount]"
		display_template {
			@multirow.work_amount_pretty;noquote@
		}
	}
} else {
	lappend element_list {
		label "[im_category_from_id [lindex $uom_ids 0]]"
		display_template {
			@multirow.work_amount_pretty;noquote@
		}
	}
}

lappend element_list end_date {
	label "[_ sencha-freelance-translation.Deadline]"
	display_template {
		@multirow.deadline_pretty;noquote@
	}
}



template::list::create \
-name "assignee_projects" \
-multirow multirow \
-key assignee_id \
-elements $element_list


set extend_list [list project_nr_pretty work_amount_pretty deadline_pretty]

db_multirow -extend $extend_list multirow multirow "select distinct p.project_nr, p.project_id, p.project_name,
	min(fa.end_date) as deadline, sum(assignment_units) as work_amount, min(uom_id) as uom_id
	from im_projects p, im_freelance_packages fp, im_freelance_assignments fa
	where fa.assignee_id = :assignee_id
	and fp.project_id = p.project_id
	and fp.freelance_package_id = fa.freelance_package_id
	and fa.assignment_status_id in ([template::util::tcl_to_sql_list $assignment_status_ids])
	and p.project_status_id in ([template::util::tcl_to_sql_list [im_sub_categories [im_project_status_open]]])
	group by p.project_nr, p.project_id, p.project_name
" {
	set assignments_url [export_vars -base "/sencha-freelance-translation/project-assignments" -url {assignee_id project_id}]
	set project_nr_pretty "<a href='$assignments_url'>$project_nr</a>"
		
	set deadline_pretty [lc_time_fmt $deadline "%q %X"]

	set work_amount_pretty ""
	if {[llength $uom_ids]>1} {
		# We need to check if this project has multiple uom_ids
		db_foreach work_amount "select uom_id, sum(assignment_units) as work_amount
			from im_freelance_assignments fa, im_freelance_packages fp
			where fa.assignee_id = :assignee_id
			and fp.freelance_package_id = fa.freelance_package_id
			and fp.project_id = :project_id
			and fa.assignment_status_id in ([template::util::tcl_to_sql_list $assignment_status_ids])
			group by uom_id" {
				append work_amount_pretty "$work_amount [im_category_from_id $uom_id] <br/>"
			}
	} else {
		append work_amount_pretty "$work_amount"
	}
		
	set assignment_ids [db_list assignment_ids "select assignment_id
		from im_freelance_assignments fa, im_freelance_packages fp
		where fa.assignee_id = :assignee_id
		and fa.freelance_package_id = fp.freelance_package_id
		and fp.project_id = :project_id
		and fa.assignment_status_id in ([template::util::tcl_to_sql_list $assignment_status_ids])"]

	set viewed_p 1
	foreach assignment_id $assignment_ids {
		if {![views::viewed_p -object_id $assignment_id -user_id $user_id]} {
			set viewed_p 0
		}
	}
	if {!$viewed_p} {
		set project_nr_pretty "<b>$project_nr_pretty</b>"
	}
}
template::multirow sort multirow project_nr
	