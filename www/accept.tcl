ad_page_contract {
    @author malte.sussdorff@cognovis.de
} {
    assignee_id:integer,notnull
    assignment_id:integer,notnull
    project_id:integer,notnull
}

set user_id [im_require_login]

# Check that the assignment is still available
# And no other user has been assigned for tasks in this assignment

db_1row assignment_info "select freelance_package_id, assignment_type_id, rate, uom_id,end_date as deadline from im_freelance_assignments where assignment_id = :assignment_id"

set existing_assignee_id [db_string assigned_p "select assignee_id from im_freelance_assignments where freelance_package_id = :freelance_package_id
    and assignment_type_id = :assignment_type_id
    and assignment_status_id in (4222,4224,4225,4226) limit 1" -default 0]

set token [ns_sha1 someSeCrEt-${assignee_id}-${project_id}]
set assignments_link [export_vars -base "/sencha-freelance-translation/project-assignments" -url {token project_id assignee_id}]

if {$existing_assignee_id >0} {
    # Check if he is the assigned
    if {$assignee_id eq $existing_assignee_id} {
        ad_returnredirect $assignments_link
    } else {
        ad_return_error "Assignment no longer available" "We are sorry to inform you, but the assignment is no longer available."
        ad_script_abort
    }
}

# We first add the freelancer as a member to the project.
set role_id 1300 ;# default role_id for full member
        
set task_type [string tolower [im_category_from_id -translate_p 0 $assignment_type_id]]
set provider_id [db_string select_company {
    select  c.company_id as provider_id
    from            acs_rels r,
            im_companies c
    where       r.object_id_one = c.company_id
    and     r.object_id_two = :assignee_id
    limit 1
} -default 0]

db_1row project_info "select project_nr,project_lead_id from im_projects where project_id = :project_id"

set task_ids [db_list tasks "select trans_task_id from im_freelance_packages_trans_tasks where freelance_package_id = :freelance_package_id"]

# Update the status to assigned
db_dml update "update im_freelance_assignments set assignment_status_id = 4222 where assignment_id = :assignment_id"

set project_member_ids [im_biz_object_member_ids $project_id]
set user_id $assignee_id
if {[lsearch $project_member_ids $user_id]<0} {
    callback im_before_member_add -user_id $user_id -object_id $project_id
    im_biz_object_add_role $user_id $project_id $role_id
    im_audit -object_id $project_id -action "after_update" -comment "After adding members"
    set touched_p 1
}
	                
set task_ids [db_list tasks_from_assignment "select trans_task_id from im_freelance_packages_trans_tasks fptt, im_freelance_assignments fa where fa.freelance_package_id = fptt.freelance_package_id and fa.assignment_id = :assignment_id and assignment_type_id = :assignment_type_id"]

# Update the tasks
foreach task_id $task_ids {
    db_dml update "update im_trans_tasks set ${task_type}_id = :user_id , ${task_type}_end_date = to_date(:deadline,'YYYY-MM-DD HH24:MI') where task_id = $task_id"
}
	                                
set invoice_id [im_freelance_create_purchase_orders -assignment_id $assignment_id]
    
db_dml update_other "update im_freelance_assignments set assignment_status_id = 4228 where assignment_id in (select assignment_id from im_freelance_assignments where freelance_package_id = :freelance_package_id
    and assignment_type_id = :assignment_type_id
    and assignment_status_id in (4220,4221))"


# Check that we have packages ready for the target language
set target_language_id [db_string target "select target_language_id from im_trans_tasks where task_id = :task_id"]

# emails freelancer the invoice
im_invoice_send_invoice_mail -invoice_id $invoice_id -recipient_id $assignee_id -from_addr [party::email -party_id $project_lead_id]

# Notify the project_lead
set assignments_url [export_vars -base "/sencha-freelance-translation/project-assignments" -url {project_id assignee_id}]
im_freelance_notify -object_id $assignment_id -recipient_ids $project_lead_id \
    -message "<a href='$assignments_url'>$project_nr</a> :: $task_type accepted by [im_name_from_id $assignee_id] for [im_category_from_id $target_language_id]" \
    -severity 2


### Inform the assignee he can start the work ###

# We need to check if file for that freelance_package_id exist.
# First we check in database
set package_filepath [db_string get_package_filepath "select file_path from im_freelance_package_files where freelance_package_id=:freelance_package_id" -default ""]

if {$package_filepath ne ""} {
    # Next we also check if file was saved on server
    if {[file exists $package_filepath]} {
        # We set required variables
        set location [util_current_location]
        set locale [lang::user::locale -user_id $assignee_id]
        set internal_company_name [im_name_from_id [im_company_freelance]]
        set salutation_pretty [im_invoice_salutation -person_id $assignee_id]
        # Set the assignment status to work ready (4229)
        db_dml update_ass_status "update im_freelance_assignments set assignment_status_id = 4229 where assignment_id = :assignment_id"

        set token [ns_sha1 someSeCrEt-${assignee_id}-${project_id}]
        set assignments_link [export_vars -base "${location}/sencha-freelance-translation/project-assignments" -url {token project_id assignee_id}]
        set download_url [export_vars -base "${location}/sencha-freelance-translation/package-download" -url {freelance_package_file_id}]
        set package_url $download_url

        # Getting and setting deadline
        set assignment_deadline [db_string assignment_deadline "select end_date from im_freelance_assignments where assignment_id =:assignment_id" -default ""]
        set assignment_deadline_formatted [lc_time_fmt $assignment_deadline "%q %X"]

        set subject "[lang::message::lookup $locale sencha-freelance-translation.lt_package_ready_message_subject]"
        set body "[lang::message::lookup $locale sencha-freelance-translation.lt_package_ready_message_body]"

        set signature [db_string signature "select signature from parties where party_id = :project_lead_id" -default ""]
        if {$signature ne ""} {
            append body [template::util::richtext::get_property html_value $signature]
        }


				intranet_chilkat::send_mail \
					-to_party_ids $assignee_id \
					-from_party_id $project_lead_id \
					-subject $subject \
					-body $body \
					-no_callback 

        im_freelance_notify \
        -object_id $assignment_id \
        -recipient_ids $assignee_id   
    } 
} 


ad_returnredirect $assignments_link
