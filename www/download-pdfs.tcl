# /packages/intranet-trans-trados/trados/download-pdfs.tcl
#
# Copyright (c) 2016, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
    Purpose: Download all Source Files as PDFS
    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
    @creation-date 2016-06-15
} {
    {project_id ""}
}

# -----------------------------------------------------------
# Defaults
# -----------------------------------------------------------
set user_id [ad_maybe_redirect_for_registration]

    
set perm_p 0
# Check if the user has admin rights on the parent_id
# to allow freelancers to add sub-projects
im_project_permissions $user_id $project_id view read write admin
if {$admin} { set perm_p 1 }
    

# ---------------------------------------------------------------------
# Get some more information about the project
# ---------------------------------------------------------------------

set project_query "
    select
        p.project_nr,
        p.source_language_id,
        company_id,
        final_company_id,
        subject_area_id
    from
        im_projects p
    where
        p.project_id=:project_id
"

if { ![db_0or1row projects_info_query $project_query] } {
    ad_return_complaint 1 "[_ intranet-translation.lt_Cant_find_the_project]"
    return
}

set source_language [im_category_from_id -translate_p 0 $source_language_id]

set folder [im_trans_task_folder -project_id $project_id -source_language $source_language -folder_type "source"]
set source_path "[im_filestorage_project_path $project_id]/$folder"

set source_file_list [glob -nocomplain -directory "${source_path}" "*"]

if {$source_file_list ne ""} {
    set pdf_dir "[im_filestorage_project_path $project_id]/PDF"
    
    file delete -force $pdf_dir
    file mkdir $pdf_dir
}

foreach source_file $source_file_list {
    set file_extension [file extension $source_file]
    ds_comment "file:: $source_file :: $file_extension"
    switch $file_extension {
        ".sdlxliff" {
            # Do nothing
        }
        ".pdf" {
            # Just copy
            file copy -force $source_file $pdf_dir
        }
        default {
            # Convert them to PDF using OpenOffice
            set conversion_result [intranet_oo::convert_to -oo_file $source_file -outdir $pdf_dir]
#            if {[lindex $conversion_result 0] eq 0} {
                # There was an error, copy the source file
#                file copy -force $source_file $pdf_dir
#            }
        }
    }
}

if {$source_file_list ne ""} {
    # Zip the PDF folder and return it.
    set zipfile /tmp/PDF_${project_nr}.zip
    intranet_chilkat::create_zip -directories "$pdf_dir" -zipfile $zipfile
    
    # Return the file
    set outputheaders [ns_conn outputheaders]
    ns_set cput $outputheaders "Content-Disposition" "attachment; filename=\"PDF_${project_nr}.zip\""
    ns_returnfile 200 application/zip $zipfile
    
    exec rm -rf $zipfile
} else {
    ad_return_error "No Source files" "We could not find any source files to generate PDFs for"
}